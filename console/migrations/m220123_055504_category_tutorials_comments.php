<?php

use yii\db\Migration;

/**
 * Class m220123_055504_category_tutorials_comments
 */
class m220123_055504_category_tutorials_comments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category_tutorial_comment}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'user_name' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'comment' => $this->string()->notNull(),
            'rating'  => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->timestamp()->defaultValue(null),
            'updated_at' => $this->timestamp()->defaultValue(null)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category_tutorial_comment}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220123_055504_category_tutorials_comments cannot be reverted.\n";

        return false;
    }
    */
}