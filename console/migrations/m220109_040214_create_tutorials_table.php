<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tutorials}}`.
 */
class m220109_040214_create_tutorials_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tutorials}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'tutorial_video' => $this->string()->notNull(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tutorials}}');
    }
}