<?php

namespace backend\models;

use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $category_name
 * @property string $category_slug
 * @property int $created_at
 * @property int $updated_at
 */
class Categories extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'category_slug',], 'required'],
            [['category_image'], 'required', 'on' => 'create'],
            [['category_name', 'category_slug'], 'string', 'max' => 255],
            [['category_name'], 'unique'],
            [['category_slug'], 'unique'],
            [['category_image'], 'file'],
            // [['category_image'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'on' => 'create-image'],
            [['category_image'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true, 'on' => 'update-image'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
            'category_slug' => 'Category Description',
            'category_image' => 'Category Image'
        ];
    }
}