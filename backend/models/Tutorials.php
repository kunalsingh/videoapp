<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tutorials".
 *
 * @property int $id
 * @property int $category_id
 * @property string $tutorial_video
 */
class Tutorials extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tutorials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['tutorial_video'], 'required', 'on' => 'create'],
            [['tutorial_video'], 'file',  'maxFiles' => 20],
            [['category_id'], 'integer'],
            [['tutorial_video'], 'file',  'skipOnEmpty' => false, 'on' => 'update'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'tutorial_video' => 'Tutorial Video',
        ];
    }
}