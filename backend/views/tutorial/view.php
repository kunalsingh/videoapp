<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Tutorials */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tutorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tutorials-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])  -->
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            [
                'attribute' => 'Tutorials Video',
                'format' => 'raw',
                'label' => 'Tutorials Video',
                'value' => function ($data) {
                    return '<video width="200" height="240" controls>
                    <source src="' . Yii::$app->request->BaseUrl . '/' . $data['tutorial_video'] . '" type="video/mp4">
                  </video>';
                },
            ],
        ],
    ]) ?>

</div>