<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Categories;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\Tutorials */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tutorials-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'method' => 'post']); ?>

    <!-- <?= $form->field($model, 'category_id')->textInput() ?> -->
    <div class="form-group">
        <?= Html::activeDropDownList(
            $model,
            'id',
            ArrayHelper::map(Categories::find()->all(), 'id', 'category_name'),
            ['class' => 'form-control']
        ) ?>
    </div>

    <?= $form->field($model, 'tutorial_video[]')->fileInput(['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>