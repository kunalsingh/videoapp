<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Categories;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\TutorialsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
#w0 {
    display: flex !important;
}

.form-control {
    width: 25% !important;
    margin-right: 20px !important;
}
</style>

<div class="tutorials-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= Html::activeDropDownList(
        $model,
        'category_id',
        ArrayHelper::map(Categories::find()->all(), 'id', 'category_name'),
        ['prompt' => 'Select Category', 'class' => 'form-control']
    ) ?>
    <br />
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <!-- Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary'])  -->
    </div>

    <?php ActiveForm::end(); ?>

</div>
<br /><br />