<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use backend\models\Categories;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\TutorialsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tutorials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tutorials-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tutorials', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel]);
    ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'category_id',
            // 'tutorial_video',
            [
                'label' => 'Category Name',
                'value' => function ($data) {
                    return Categories::findOne(['id' => $data->category_id])->category_name;
                },
            ],
            [
                'attribute' => 'Tutorials Video',
                'format' => 'raw',
                'label' => 'Tutorials Video',
                'attribute' => 'source',
                // 'value' => !empty($model->tutorial_video) ? '<iframe class="embed-responsive-item" src="' . $model->tutorial_video . '" frameborder="0" allowfullscreen></iframe>' : null,
                'value' => function ($data) {
                    // return Html::img(
                    //     Yii::$app->request->BaseUrl . '/' . $data['tutorial_video'],
                    //     [
                    //         'width' => '80px',
                    //         'height' => '80px'
                    //     ]
                    // );

                    // return '<iframe class="embed-responsive-item" src="' . $data['tutorial_video'] . '" frameborder="0" allowfullscreen></iframe>';
                    return '<video width="200" height="240" controls>
                    <source src="' . Yii::$app->request->BaseUrl . '/' . $data['tutorial_video'] . '" type="video/mp4">
                  </video>';
                },

            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {delete}',
                'urlCreator' => function ($action,  $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>