<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'email',
            [
                'attribute' => 'status',
                'format' => 'raw',
                //it 'html' before  
                'value' => function ($data) {

                    $url = 'index.php?r=site/updatestatus&id=' . $data->id;
                    if ($data->status == 9) {
                        // return Html::Button(
                        //     "inactive",
                        //     ['id' => $data->id, 'class' => 'a_status', 'method' => 'post', "onclick" => "updatestatus('" . $data->id . "')"],

                        // );
                        return Html::a('InActive', $url, array('data' => [
                            'confirm' => 'Are you sure you want to Update?',
                            'method' => 'post'
                        ]));
                    } else {
                        // return Html::Button(
                        //     "active",
                        //     ['id' => $data->id, 'class' => 'a_status', 'method' => 'post', "onclick" => "updatestatus('" . $data->id . "')"],

                        // );
                        return Html::a('Active', $url, array('data' => [
                            'confirm' => 'Are you sure you want to Update?',
                            'method' => 'post'
                        ]));
                    }
                }
                //missing parenthesis 
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
            ],
            // [
            //     'class' => ActionColumn::className(),
            //     'urlCreator' => function ($action,  $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'id' => $model->id]);
            //     }
            // ],
        ],
    ]); ?>


</div>

<script language="javascript" type="text/javascript">
// function updatestatus(id) {

//     $.ajax({

//         url: '"<?= Yii::$app->urlManager->createUrl("updatestatus ") ?>/"' + id,


//     }).done(function(data) {

//         $.fn.yiiGridView.update("posts-grid");

//     });

// }
</script>