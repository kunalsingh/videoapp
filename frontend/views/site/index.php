<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Categories;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */

$this->title = 'Course List';
?>
<style>
#w0 {
    display: flex !important;
}

.form-control {
    /* width: 25% !important; */
    margin-right: 20px !important;
}

.category_search {
    width: 200px;
}
</style>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Courses!</h1>



        <div class="tutorials-search">

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <h2>Category : </h2>
            <div class="category_search">

                <select name="category" class="form-control">
                    <option value="">Select Category</option>
                    <?php $selected = ""; ?>
                    <?php foreach ($catdrop as $cat) {

                    ?>
                    <option value="<?= $cat->id; ?>" <?php if ($cat->id == @$_GET['category']) {
                                                                echo "selected";
                                                            } ?>><?= $cat->category_name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <br />
            <div class=" form-group" style="margin-left: 20px">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <br /><br />
    </div>

    <div class="body-content">

        <div class="row">
            <?php foreach ($category as $cat) { ?>

            <div class="card" style="width: 20rem;margin:20px;">
                <?php if (Yii::$app->user->isGuest) { ?>
                <img src="<?= Yii::$app->params['backendUrl'] . '/' . $cat->category_image ?>" class="card-img-top"
                    alt="...">
                <div class="card-body">
                    <h5 class="card-title"><?= $cat->category_name; ?></h5>
                    <p class="card-text"><?= $cat->category_slug; ?></p>

                </div>
                <?php } else { ?>
                <a href="<?= '?r=site%2Ftutorial&id=' . $cat->id; ?>">
                    <img src="<?= Yii::$app->params['backendUrl'] . '/' . $cat->category_image ?>" class="card-img-top"
                        alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?= $cat->category_name; ?></h5>
                        <p class="card-text"><?= $cat->category_slug; ?></p>

                    </div>
                </a>
                <?php } ?>
            </div>
            <?php } ?>

        </div>

    </div>
</div>