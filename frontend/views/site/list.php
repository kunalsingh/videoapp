<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'List Category';
?>
<style>
.help-block {
    color: red !important;
}
</style>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= ucfirst($name); ?> All Video </h1>
        <!-- <p class="lead">You have successfully created your Yii-powered application.</p> -->
        <?= Html::a('Go Category List', ['/site/index'], ['class' => 'btn btn-primary', 'style' => 'float: right;']) ?>
    </div>

    <div class="body-content">

        <div class="row">
            <?php if (!empty($tutorial)) { ?>
            <?php foreach ($tutorial as $tut) { ?>
            <div class="card" style="width: 22rem;margin:10px;">

                <video controls>
                    <source src="<?= Yii::$app->params['backendUrl'] . '/' . $tut->tutorial_video ?>" type="video/mp4">
                </video>
                <div class="card-body">

                </div>
            </div>

            <?php } ?>

            <?php } else { ?>
            <h2> Not Record Found </h2>
            <?php } ?>
        </div>
        <br /><br />
        <hr />
        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'comment-form', 'action' => 'index.php?r=site/createcomment']); ?>
                <input type="hidden" name="id" class="id" value="<?= $id; ?>" />
                <?= $form->field($model, 'comment')->textArea(['rows' => 6]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <?php foreach ($commentlist as $com) { ?>
        <div class="comment">

            <h3><?= $com->comment; ?></h3>

            <p style="text-align: right;">Author:- <?= $com->user_name; ?></p>

        </div>
        <hr />

        <?php } ?>

    </div>