<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "category_tutorial_comment".
 *
 * @property int $id
 * @property int $category_id
 * @property string $user_name
 * @property int $user_id
 * @property string $comment
 * @property int $rating
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class CategoryTutorialComment extends \yii\db\ActiveRecord

{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_tutorial_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'user_name', 'user_id', 'comment', 'rating'], 'required'],
            [['category_id', 'user_id', 'rating', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_name', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'user_name' => 'User Name',
            'user_id' => 'User ID',
            'comment' => 'Comment',
            'rating' => 'Rating',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}